import curses
import ui
from assets import get_problem
from sudoku_tests import ex

def main(stdscreen):
    
    terminal_ui = ui.SudokuUI(stdscreen)
    
    grid = get_problem(0)
    
    terminal_ui.print_problem(grid)
    terminal_ui.wait_for_input()
    
    # write main loop here

if __name__ == "__main__":    
	curses.wrapper(main)
